from pyfirestore import Collection, Document, CustomExceptions, get_client


get_client("pyfirestore/tests/testuser.json")

# Creates a collection
col = Collection("My collection")

# Creates a document (not stored in Firestore's server yet)
doc = Document(collection=col, first=1)

# Pushes it to the Firestore server
doc.push()

# Changes its content, directly on server
doc.update(first="first argument", second="second argument")

# Change its content locally, then pushing the changes
doc.third = "Third argument"
doc.update()

# Gets the document by its id
found_doc = col.get(doc.id)
print("Fetched doc :", found_doc)

# Trying to get a document that doesn't exist raises an exception
try:
    found_doc = col.get("00000-11111")
except CustomExceptions.NotFound:
    pass
else:
    print("The document shouldn't have been found")

# Searching a list of documents
found_docs = col.search(first="first argument")
print("Found {} docs : {}".format(len(found_docs), found_docs))

# Comparing docs
assert doc == found_doc

# Create a subcollection
subcol = Collection("Sub collection", document=doc)

# And a document inside that subcollection
subcol_doc = Document(foo="bar", collection=subcol)
subcol_doc.push()

# Deletes documents
subcol_doc.delete()
doc.delete()
