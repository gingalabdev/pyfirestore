import json
import uuid
from unittest import TestCase, mock

from pyfirestore import Collection, Document, CustomExceptions


class TestDocument(TestCase):
    def setUp(self):
        super().setUp()
        self.col = Collection("test_collection")

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def tearDown(self):
        super().tearDown()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def test_setter(self):
        doc = Document(collection=self.col)
        self.assertNotIn('some_property', doc._properties)
        doc.some_property = "my value"
        self.assertEqual(doc.some_property, "my value")
        self.assertIn('some_property', doc._properties)

    def test_id_can_be_readonly(self):
        with self.subTest("Readonly when constructed with is_new=False"):
            doc = Document(collection=self.col, is_new=False, id="123")
            with self.assertRaises(CustomExceptions.Forbidden):
                doc.id = "some other id"

        with self.subTest("Readonly when pushed"):
            doc = Document(collection=self.col)
            with mock.patch.object(doc._f_document, 'set'):
                doc.push()

            with self.assertRaises(CustomExceptions.Forbidden):
                doc.id = "some other id"

        with self.subTest("Not readonly when not pushed yet"):
            doc = Document(collection=self.col)
            doc.id = "some other id"  # Should not raise

    def test_instanciation(self):
        with self.subTest("Test with some data"):
            doc = Document(collection=self.col, one=1, two=2)  # Should not raise
            self.assertEqual(doc.collection, self.col)
            self.assertEqual(doc.one, 1)
            self.assertEqual(doc.two, 2)

        with self.subTest("Test without any specific data"):
            Document(collection=self.col)  # Should not raise

        with self.subTest("Test missing collection argument"):
            with self.assertRaises(CustomExceptions.MandatoryArgument):
                Document(one=1, two=2)

        with self.subTest("Test wrong type for collection argument"):
            with self.assertRaises(CustomExceptions.TypeError):
                Document(collection=1, one=1, two=2)

        with self.subTest("Creating automatically a random id"):
            with mock.patch("pyfirestore.uuid.uuid4") as mocked:
                mocked.return_value = uuid.UUID('00000000-0a0e-4791-99ef-d21466cc1f32')
                doc = Document(collection=self.col, one=1, two=2)
                self.assertEqual(doc.id, '00000000-0a0e-4791-99ef-d21466cc1f32')
                self.assertTrue(doc._is_new)

        with self.subTest("Using provided uuid"):
            doc = Document(collection=self.col, one=1, two=2, id='11111111-0a0e-4791-99ef-d21466cc1f32')
            self.assertEqual(doc.id, '11111111-0a0e-4791-99ef-d21466cc1f32')
            self.assertTrue(doc._is_new)

        with self.subTest("Creation of an existing Document with a newly generated id"):
            with self.assertRaises(CustomExceptions.MandatoryArgument):
                Document(collection=self.col, is_new=False)  # Shouldn't be possible

    def test_equality(self):
        self.assertEqual(Document(collection=self.col, one=1, two=2, id="123"),
                         Document(collection=self.col, one=1, two=2, id="123"))

    def test_push(self):
        with self.subTest("Inserts the id as a field of the document"):
            # It's necessary to have at least 1 field for the document to be retrievable
            doc = Document(collection=self.col)
            with mock.patch.object(doc._f_document, 'set') as patched:
                doc.push()

            patched.assert_called_once_with(dict(id=doc.id))

        with self.subTest("New element, has to be pushed to the server"):
            doc = Document(collection=self.col, one=1, two=2)
            self.assertTrue(doc._is_new)  # it is a new entry
            with mock.patch.object(doc._f_document, 'set') as patched:
                doc.push()

            patched.assert_called_once_with(dict(id=doc.id, one=1, two=2))
            self.assertFalse(doc._is_new)  # it is not a new entry anymore

        with self.subTest("Existing element, we should get an error"):
            doc = Document(collection=self.col, one=1, two=2, id="123456", is_new=False)
            with self.assertRaises(Exception):
                doc.push()

        with self.subTest("Can force a push (to select the id locally)"):
            doc = Document(collection=self.col, one=1, two=2, id="123456")
            with mock.patch.object(doc._f_document, 'set') as patched:
                doc.push(force=True)

            patched.assert_called_once()

        with self.subTest("Pushing a doc in a subcollection also pushes the parent documents that have not been pushed "
                          "yet"):
            doc = Document(collection=self.col, one=1, two=2, id="123456")
            subcol = Collection("Test", document=doc)
            subdoc = Document(collection=subcol)
            with mock.patch.object(doc._f_document, 'set') as patched1:
                with mock.patch.object(subdoc._f_document, 'set') as patched2:
                    subdoc.push()

            patched1.assert_called_once()
            patched2.assert_called_once()

    def test_update(self):
        with self.subTest("Works without arguments"):
            doc = Document(collection=self.col, one=1, two=2)
            with mock.patch.object(doc, 'push'):
                doc.update()  # Should not raise

        with self.subTest("Can add new arguments"):
            doc = Document(collection=self.col, one=1, two=2)
            doc.update(ten="ten")

        with self.subTest("New element, has to be pushed to the server first"):
            doc = Document(collection=self.col, one=1, two=2)
            with mock.patch.object(doc, 'push') as patched:
                doc.update()

            patched.assert_called_once()

        with self.subTest("Existing element, has to be updated on the server"):
            doc = Document(collection=self.col, one=1, two=2, id="123456", is_new=False)
            with mock.patch.object(doc._f_document, 'update') as patched:
                doc.update()

            patched.assert_called_once()

        with self.subTest("The element's properties have been updated"):
            doc = Document(collection=self.col, one=1, two=2)
            with mock.patch.object(doc, 'push'):
                doc.update(one="one", two="two")

            self.assertEqual(doc.one, "one")
            self.assertEqual(doc.two, "two")

        with self.subTest("Only updates the keys that have changed"):
            # This will allow to have 2 servers communicating with the same Documents, only rewriting their changes
            # and not the entire document's values
            doc = Document(collection=self.col, one=1, two=2)
            with mock.patch.object(doc._f_document, 'update') as mocked:
                # We make as if the doc had just been pushed
                doc._is_new = False
                doc._changed_fields = []

                doc.update(one="one")
                mocked.assert_called_with(dict(one="one"))

                doc.two = "two"
                doc.update()
                mocked.assert_called_with(dict(two="two"))

    def test_from_dict(self):
        doc = Document.from_dict(self.col,
                                 dict(id='4580a950-0a0e-4791-99ef-d21466cc1f32', one=1, two=2, three=3))
        self.assertEqual(str(doc.collection), str(Collection("test_collection")))
        self.assertEqual(doc.id, '4580a950-0a0e-4791-99ef-d21466cc1f32')
        self.assertEqual(doc.one, 1)
        self.assertEqual(doc.two, 2)
        self.assertEqual(doc.three, 3)

    def test_to_dict(self):
        doc = Document(collection=self.col, one=1, two=2, id='4580a950-0a0e-4791-99ef-d21466cc1f32')
        self.assertEqual(doc.to_dict(), dict(id='4580a950-0a0e-4791-99ef-d21466cc1f32', one=1, two=2))

    def test_to_json(self):
        doc = Document(collection=self.col, one=1, two=2, id="11111-22222-33333-44444")
        self.assertEqual(doc.to_json(), json.dumps(
            dict(collection=self.col.to_json(), document="11111-22222-33333-44444")))

    def test_from_json(self):
        dump = json.dumps(dict(collection=self.col.to_json(), document="11111-22222-33333-44444"))
        with mock.patch.object(Collection, 'get') as patched:
            patched.return_value = object()
            doc = Document.from_json(dump)

        patched.assert_called_once_with(id="11111-22222-33333-44444")
        self.assertIs(doc, patched.return_value)  # We are returning the provided document instance

    def test_delete(self):
        with self.subTest("New element, which doesn't exist on the server yet"):
            doc = Document(collection=self.col, one=1, two=2)
            with mock.patch.object(doc._f_document, 'delete') as patched:
                doc.delete()

            patched.assert_not_called()

        with self.subTest("Existing element, has to be removed from the server"):
            doc = Document(collection=self.col, one=1, two=2)
            with mock.patch.object(doc._f_document, 'set'):
                # We don't really want to push it in the tests
                doc.push()

            with mock.patch.object(doc._f_document, 'delete') as patched:
                doc.delete()

            patched.assert_called_once()

        with self.subTest("Delete creates a new doc with new id but same data"):
            doc = Document(collection=self.col, one=1, two=2)
            initial_id = doc.id

            with mock.patch.object(doc._f_document, 'set'):
                # We don't really want to push it in the tests
                doc.push()

            with mock.patch.object(doc._f_document, 'delete') as patched:
                doc.delete()

            self.assertNotEqual(doc.id, initial_id)
            self.assertTrue(doc._is_new)
            self.assertEqual(doc.one, 1)
            self.assertEqual(doc.two, 2)
