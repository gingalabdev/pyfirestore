import uuid
from unittest import TestCase, skip

from pyfirestore import Collection, Document, CustomExceptions


class TestEndToEnd(TestCase):

    def setUp(self):
        super().setUp()

    @skip
    def test_multiple_levels(self):
        col1 = Collection("Test collection 1 - {}".format(uuid.uuid4()))
        doc1 = Document(collection=col1, name="Doc from col 1")
        col2 = Collection("Test collection 1 - {}".format(uuid.uuid4()), document=doc1)
        doc2 = Document(collection=col2, name="Doc from col 2")
        doc2.push()
        doc3 = Document(collection=col2, name="Doc from col 3")
        doc3.push()

        doc2_json = doc2.to_json()

        # Reconstructing it from the db
        doc2_restored = Document.from_json(doc2_json)

        self.assertEqual(doc2_restored, doc2)
        self.assertEqual(doc2_restored.collection, doc2.collection)
        self.assertEqual(doc2_restored.collection.document, doc2.collection.document)
        self.assertEqual(doc2_restored.collection.document.collection, doc2.collection.document.collection)

        doc3.delete()
        with self.assertRaises(CustomExceptions.NotFound):
            col2.get(id=doc3.id)

        col1.get(id=doc1.id)  # Should not raise, the doc should still exist

        doc1.delete()
        with self.assertRaises(CustomExceptions.NotFound):
            col1.get(id=doc1.id)

    def test_one_level(self):
        col1 = Collection("Test collection 1 - {}".format(uuid.uuid4()))
        col2 = Collection("Test collection 2 - {}".format(uuid.uuid4()))
        doc1 = Document(collection=col1, one=1, two=2, three=3)
        doc1.push()
        doc2 = Document(collection=col1, one=1, two=20, three=30)
        doc2.push()
        doc3 = Document(collection=col2, one=1, two=20, three=30)
        doc3.push()
        doc4 = Document(collection=col1, one=1, two=20, three=30)  # Not pushed

        found_document = col1.get(id=doc1.id)
        self.assertEqual(found_document, doc1)

        found_documents = col1.search(one=1)
        self.assertIn(doc1, found_documents)
        self.assertIn(doc2, found_documents)

        found_documents = col1.search(one=5)
        self.assertEqual(found_documents, [])

        # Another pointer to first doc
        doc1_copy = col1.get(doc1.id)

        # Updating some of each one's data
        doc1.one = "one"
        doc1.two = "two"
        doc1.update(four="four")
        doc1_copy.update(two="forty-two", three="three")

        fresh_first_document = col1.get(id=doc1.id)
        self.assertEqual(fresh_first_document.one, "one")
        self.assertEqual(fresh_first_document.two, "forty-two")  # The second call should have replaced it
        self.assertEqual(fresh_first_document.three, "three")

        doc1.delete()
        doc2.delete()
        doc3.delete()
        doc4.delete()

        with self.assertRaises(CustomExceptions.NotFound):
            col1.get(id=doc1.id)
