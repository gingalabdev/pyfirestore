import json
from unittest import TestCase, mock

from google.cloud import exceptions

from pyfirestore import Collection, Document, CustomExceptions


class MockedReturn:
    def __init__(self, num):
        self.num = num

    def to_dict(self):
        return dict(one=self.num, two=self.num * 2, id=str(self.num), collection="a")

    def doc(self, collection=None):
        if collection is None:
            collection = Collection("a")

        return Document(one=self.num, two=self.num * 2, id=str(self.num), collection=collection, is_new=False)


class TestCollection(TestCase):
    def setUp(self):
        super().setUp()
        self.col = Collection("test_collection")
        self.doc = Document(one="1", two="2", collection=self.col)
        self.subcol = Collection("test_subcollection", document=self.doc)
        self.subdoc = Document(one="Uno", two="Dos", collection=self.subcol)

        self.col_json_data = json.dumps(dict(name="test_collection"))
        self.subcol_json_data = json.dumps(
            dict(name="test_subcollection", document=json.dumps(
                dict(collection=self.col_json_data, document=self.doc.id))))

    def tearDown(self):
        super().tearDown()

    def test_equality(self):
        self.assertEqual(Collection("test"), Collection("test"))

    def test_has_custom_string_representation(self):
        self.assertTrue("test_collection" in str(self.col),
                        "The name of the collection should be present in the representation")

    def test_to_json(self):
        with self.subTest("First level collection"):
            self.assertEqual(self.col.to_json(), self.col_json_data)

        with self.subTest("Subcollection"):
            self.assertEqual(self.subcol.to_json(), self.subcol_json_data)

    def test_from_json(self):
        with self.subTest("First level collection"):
            self.assertEqual(str(Collection.from_json(self.col_json_data)), str(self.col))

        with self.subTest("Subcollection"):
            with mock.patch("pyfirestore.Collection.get") as patched:
                patched.return_value = MockedReturn(1).doc(collection=self.col)
                col = Collection.from_json(self.subcol_json_data)

            self.assertEqual(col, self.subcol)
            self.assertEqual(col.document.collection, self.col)

    def test_search(self):
        with mock.patch.object(self.col._f_collection, 'where') as patched:
            mocked_where_return = mock.MagicMock(name="where")
            mock.MagicMock(name="get")

            mocked_where_return.get.return_value = [MockedReturn(1), MockedReturn(2)]
            patched.return_value = mocked_where_return
            ret = self.col.search(id="something")

        patched.assert_called_once_with('id', '==', "something")
        mocked_where_return.get.assert_called_once_with()
        self.assertEqual(ret[0], Document.from_dict(self.col, dict(one=1, two=2, id="1")))
        self.assertEqual(ret[1], Document.from_dict(self.col, dict(one=2, two=4, id="2")))

    def test_get(self):
        with self.subTest("Getting an existing document"):
            with mock.patch.object(self.col._f_collection, 'document') as patched:
                docref = mock.MagicMock()
                docref.get = mock.MagicMock(return_value=MockedReturn(1))
                patched.return_value = docref
                ret = self.col.get(id="1")

            self.assertEqual(ret, Document.from_dict(self.col, dict(one=1, two=2, id="1")))

        with self.subTest("Error when trying to get a document that doesn't exist"):
            with mock.patch.object(self.col._f_collection, 'document') as patched:
                docref = mock.MagicMock()
                docref.get = mock.MagicMock(side_effect=exceptions.NotFound("No such document"))
                patched.return_value = docref
                with self.assertRaises(CustomExceptions.NotFound):
                    self.col.get(id="1")

    def test_constructor(self):
        # No need to confirm the constructor accepts both a collection and a document, otherwise, the setUp would fail
        with self.subTest("Requires a collection name"):
            # TypeError instead of CustomExceptions.TypeError because it's a normal Python constructor's error
            with self.assertRaises(TypeError):
                Collection()

        with self.subTest("Parent collection"):
            self.assertEqual(self.col._f_collection._path, ("test_collection",))

        with self.subTest("Parent document should be an instance of Document"):
            with self.assertRaises(CustomExceptions.TypeError):
                Collection("something", document=5)

        with self.subTest("Sub-collection"):
            self.assertEqual(self.subcol._f_collection._path, ('test_collection', self.doc.id, 'test_subcollection'))
