from setuptools import setup


setup(
    name="PyFireStore",
    version="0.3",
    author="Brice Parent - GingaLab",
    author_email="bparent@gingalab.com",
    description="Python wrapper around Google's FireStore API.",
    description_content_type='text/markdown',
    license="Apache",
    keywords="Python FireStore",
    url="https://bitbucket.org/gingalabdev/pyfirestore",
    packages=['pyfirestore'],
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    install_requires=['google-cloud-firestore>=1.7.0,<1.8'],
)
